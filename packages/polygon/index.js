import Polygon from './src/polygon'

Text.install = function (Vue) {
  Vue.component(Polygon.name, Polygon)
}
export default Polygon
